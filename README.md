# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://fgrcar@bitbucket.org/fgrcar/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/fgrcar/stroboskop/commits/d77548ee598f6ff1d6f8d013192370cfbf2ecad4

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/fgrcar/stroboskop/commits/4ad30ecc8738d7a10fa6f14981eff5766ba5effd

Naloga 6.3.2:
https://bitbucket.org/fgrcar/stroboskop/commits/7aa6c1e23d50612616e8aa810a457b1433ab5882

Naloga 6.3.3:
https://bitbucket.org/fgrcar/stroboskop/commits/eb64cb2277a090b9e5cd7315f616d531cb535108

Naloga 6.3.4:
https://bitbucket.org/fgrcar/stroboskop/commits/bbbf9f95e512f052936c1da2774ddedf2b744ed2

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/fgrcar/stroboskop/commits/78751bd4a7bf2b655a4e9ac20be1a47674a45635

Naloga 6.4.2:
https://bitbucket.org/fgrcar/stroboskop/commits/471c3563af3020470acb6e2f73d8faac26509775

Naloga 6.4.3:
https://bitbucket.org/fgrcar/stroboskop/commits/a915bcd88bbb082dc617abaaa2126c68c9afc062

Naloga 6.4.4:
https://bitbucket.org/fgrcar/stroboskop/commits/06b42723ba2142c5b89cafcd4810ba43a50863ac